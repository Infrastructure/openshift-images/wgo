worker_processes  4;

error_log  /dev/stderr warn;
pid        /tmp/nginx.pid;

include /usr/share/nginx/modules-available/*.conf;

events {
    worker_connections  1024;
}

http {
    proxy_temp_path /tmp/proxy_temp;
    client_body_temp_path /tmp/client_temp;
    fastcgi_temp_path /tmp/fastcgi_temp;
    uwsgi_temp_path /tmp/uwsgi_temp;
    scgi_temp_path /tmp/scgi_temp;

    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    gzip on;
    gzip_vary on;
    gzip_types text/plain text/css text/javascript image/svg+xml image/x-icon application/javascript application/x-javascript;

    upstream php-fpm {
        server localhost:9000;
    }

    geoip_country /usr/share/GeoIP/GeoIP.dat;
    geoip_proxy 10.0.0.0/8;
    map $geoip_country_code $allowed_country {
        default yes;
        IR no;
        KP no;
        SD no;
        CU no;
        SY no;
    }

    real_ip_header X-Forwarded-For;
    set_real_ip_from 10.0.0.0/8;
    # AS8075, MICROSOFT-CORP-MSN-AS-BLOCK, 2022-12-22T10:46:19.678126
    deny 20.45.128.0/20;
    deny 20.192.0.0/10;
    deny 137.116.0.0/16;
    deny 103.249.63.0/24;
    deny 51.124.0.0/16;
    deny 198.8.73.0/24;
    deny 113.197.64.0/24;
    deny 20.136.0.0/17;
    deny 40.80.0.0/12;
    deny 52.160.0.0/11;
    deny 213.218.48.0/22;
    deny 132.245.0.0/16;
    deny 70.156.0.0/15;
    deny 216.73.183.0/24;
    deny 199.242.32.0/20;
    deny 199.65.28.0/24;
    deny 51.144.0.0/15;
    deny 40.110.0.0/15;
    deny 194.41.18.0/24;
    deny 20.74.128.0/17;
    deny 202.89.224.0/21;
    deny 62.12.59.0/24;
    deny 111.221.16.0/21;
    deny 51.136.0.0/15;
    deny 192.40.79.0/24;
    deny 150.171.254.0/24;
    deny 52.112.0.0/14;
    deny 103.249.62.0/24;
    deny 20.46.32.0/24;
    deny 20.157.0.0/16;
    deny 192.100.128.0/22;
    deny 74.248.0.0/15;
    deny 207.46.64.0/18;
    deny 170.114.47.0/24;
    deny 208.84.0.0/21;
    deny 102.37.0.0/17;
    deny 199.30.16.0/20;
    deny 40.104.0.0/15;
    deny 198.47.13.0/24;
    deny 4.160.0.0/12;
    deny 40.126.0.0/18;
    deny 208.75.36.0/22;
    deny 145.46.160.0/24;
    deny 111.221.29.0/24;
    deny 74.80.229.0/24;
    deny 185.154.81.0/24;
    deny 62.12.58.0/24;
    deny 72.144.0.0/14;
    deny 104.40.0.0/13;
    deny 23.102.0.0/16;
    deny 66.178.149.0/24;
    deny 52.96.38.0/24;
    deny 94.143.107.0/24;
    deny 51.51.0.0/16;
    deny 192.84.161.0/24;
    deny 108.140.0.0/14;
    deny 20.203.0.0/17;
    deny 185.76.37.0/24;
    deny 102.133.0.0/16;
    deny 172.200.0.0/13;
    deny 192.100.104.0/21;
    deny 62.12.57.0/24;
    deny 52.239.232.0/24;
    deny 74.160.0.0/14;
    deny 198.49.8.0/24;
    deny 51.116.0.0/16;
    deny 132.164.0.0/16;
    deny 131.253.128.0/17;
    deny 51.140.0.0/14;
    deny 185.135.57.0/24;
    deny 69.52.193.0/24;
    deny 20.160.0.0/12;
    deny 208.84.3.0/24;
    deny 208.80.21.0/24;
    deny 40.120.0.0/14;
    deny 40.66.0.0/17;
    deny 131.253.5.0/24;
    deny 40.76.0.0/14;
    deny 40.123.224.0/20;
    deny 74.224.0.0/14;
    deny 157.56.0.0/16;
    deny 40.107.142.0/23;
    deny 111.221.24.0/22;
    deny 138.128.251.0/24;
    deny 198.200.130.0/24;
    deny 203.32.10.0/24;
    deny 150.171.0.0/16;
    deny 41.223.11.0/24;
    deny 40.95.238.0/23;
    deny 134.170.0.0/16;
    deny 204.79.179.0/24;
    deny 113.197.66.0/24;
    deny 98.70.0.0/15;
    deny 20.152.0.0/16;
    deny 192.100.112.0/21;
    deny 185.117.230.0/24;
    deny 198.22.205.0/24;
    deny 72.152.0.0/14;
    deny 102.37.128.0/17;
    deny 52.224.0.0/11;
    deny 62.12.61.0/24;
    deny 20.37.64.0/19;
    deny 131.253.61.0/24;
    deny 203.84.135.0/24;
    deny 40.119.160.0/19;
    deny 20.46.32.0/19;
    deny 66.119.144.0/20;
    deny 102.37.0.0/16;
    deny 204.14.180.0/22;
    deny 198.22.19.0/24;
    deny 131.253.8.0/24;
    deny 46.29.242.0/24;
    deny 195.114.140.0/24;
    deny 45.143.225.0/24;
    deny 20.135.0.0/16;
    deny 185.154.83.0/24;
    deny 68.154.0.0/15;
    deny 192.197.157.0/24;
    deny 23.103.160.0/20;
    deny 198.51.0.0/24;
    deny 206.191.224.0/19;
    deny 40.126.128.0/17;
    deny 208.75.32.0/22;
    deny 70.37.128.0/18;
    deny 157.31.0.0/16;
    deny 205.143.44.0/24;
    deny 204.79.195.0/24;
    deny 23.103.64.0/18;
    deny 137.117.0.0/16;
    deny 51.53.0.0/16;
    deny 4.224.0.0/12;
    deny 40.126.192.0/23;
    deny 52.96.0.0/14;
    deny 113.197.67.0/24;
    deny 51.12.0.0/15;
    deny 74.200.130.0/24;
    deny 94.143.106.0/24;
    deny 208.84.1.0/24;
    deny 168.62.0.0/15;
    deny 103.164.237.0/24;
    deny 208.76.45.0/24;
    deny 40.74.0.0/15;
    deny 135.149.0.0/16;
    deny 69.52.198.0/24;
    deny 94.143.108.0/24;
    deny 185.195.244.0/24;
    deny 137.135.0.0/16;
    deny 194.180.131.0/24;
    deny 20.45.64.0/20;
    deny 199.103.122.0/24;
    deny 202.22.173.0/24;
    deny 51.107.0.0/16;
    deny 20.36.0.0/14;
    deny 20.0.0.0/11;
    deny 192.84.160.0/24;
    deny 91.216.184.0/24;
    deny 111.221.64.0/18;
    deny 94.245.64.0/18;
    deny 20.64.0.0/10;
    deny 169.138.0.0/16;
    deny 4.240.0.0/12;
    deny 204.79.135.0/24;
    deny 40.95.86.0/23;
    deny 199.65.243.0/24;
    deny 69.52.199.0/24;
    deny 128.94.0.0/16;
    deny 104.208.0.0/13;
    deny 45.143.224.0/24;
    deny 52.148.0.0/14;
    deny 207.46.48.0/20;
    deny 203.84.134.0/24;
    deny 185.209.208.0/24;
    deny 198.180.97.0/24;
    deny 20.143.0.0/16;
    deny 20.184.0.0/13;
    deny 150.171.0.0/24;
    deny 40.66.166.0/24;
    deny 52.125.0.0/16;
    deny 192.100.120.0/21;
    deny 20.40.0.0/13;
    deny 111.221.30.0/23;
    deny 20.33.0.0/16;
    deny 178.255.242.0/24;
    deny 131.253.6.0/24;
    deny 13.64.0.0/11;
    deny 51.120.0.0/16;
    deny 74.176.0.0/14;
    deny 172.208.0.0/13;
    deny 138.128.250.0/24;
    deny 185.154.82.0/24;
    deny 74.240.0.0/14;
    deny 191.232.0.0/13;
    deny 185.154.80.0/24;
    deny 195.8.43.0/24;
    deny 131.253.24.0/21;
    deny 4.144.0.0/12;
    deny 40.127.0.0/19;
    deny 161.69.104.0/24;
    deny 40.162.0.0/16;
    deny 40.112.0.0/13;
    deny 13.104.0.0/14;
    deny 20.46.192.0/19;
    deny 135.130.0.0/16;
    deny 131.253.32.0/20;
    deny 51.132.0.0/16;
    deny 20.153.0.0/16;
    deny 20.150.0.0/15;
    deny 195.105.26.0/24;
    deny 198.185.5.0/24;
    deny 208.76.46.0/24;
    deny 66.178.148.0/24;
    deny 70.152.0.0/15;
    deny 212.46.57.0/24;
    deny 40.127.0.0/16;
    deny 4.192.0.0/12;
    deny 147.145.0.0/16;
    deny 104.146.128.0/17;
    deny 103.164.236.0/24;
    deny 142.0.188.0/24;
    deny 216.220.208.0/20;
    deny 23.103.128.0/17;
    deny 23.96.0.0/14;
    deny 155.62.0.0/16;
    deny 212.1.219.0/24;
    deny 20.45.80.0/24;
    deny 170.165.0.0/16;
    deny 159.128.0.0/16;
    deny 158.158.0.0/16;
    deny 138.105.0.0/16;
    deny 208.84.4.0/24;
    deny 168.61.0.0/16;
    deny 68.210.0.0/15;
    deny 198.52.0.0/24;
    deny 52.120.0.0/14;
    deny 172.160.0.0/11;
    deny 185.209.209.0/24;
    deny 20.48.0.0/12;
    deny 199.242.48.0/21;
    deny 157.55.0.0/16;
    deny 148.7.0.0/16;
    deny 40.68.0.0/14;
    deny 40.64.0.0/15;
    deny 170.114.39.0/24;
    deny 4.176.0.0/12;
    deny 216.32.180.0/22;
    deny 20.46.144.0/20;
    deny 212.1.222.0/24;
    deny 131.253.12.0/22;
    deny 74.234.0.0/15;
    deny 40.67.0.0/16;
    deny 199.65.251.0/24;
    deny 94.143.105.0/24;
    deny 147.243.0.0/16;
    deny 40.108.128.0/17;
    deny 204.95.96.0/20;
    deny 40.107.18.0/23;
    deny 205.143.45.0/24;
    deny 40.96.0.0/13;
    deny 68.220.0.0/15;
    deny 199.65.247.0/24;
    deny 207.46.36.0/22;
    deny 194.110.197.0/24;
    deny 193.221.113.0/24;
    deny 207.46.0.0/19;
    deny 52.105.196.0/23;
    deny 23.100.0.0/15;
    deny 138.239.0.0/16;
    deny 212.1.223.0/24;
    deny 51.10.0.0/15;
    deny 52.96.0.0/12;
    deny 102.133.128.0/17;
    deny 204.152.140.0/23;
    deny 194.41.21.0/24;
    deny 65.52.0.0/14;
    deny 40.124.0.0/16;
    deny 207.46.40.0/21;
    deny 51.138.0.0/16;
    deny 52.152.0.0/13;
    deny 40.104.0.0/14;
    deny 104.47.19.0/24;
    deny 213.199.128.0/18;
    deny 208.66.228.0/24;
    deny 207.46.128.0/17;
    deny 40.120.0.0/20;
    deny 198.180.95.0/24;
    deny 207.68.128.0/18;
    deny 51.104.0.0/15;
    deny 52.136.0.0/13;
    deny 68.218.0.0/15;
    deny 212.1.218.0/24;
    deny 202.22.167.0/24;
    deny 64.4.0.0/18;
    deny 40.125.0.0/17;
    deny 78.25.1.0/24;
    deny 138.91.0.0/16;
    deny 208.84.2.0/24;
    deny 208.68.136.0/21;
    deny 113.197.65.0/24;
    deny 13.107.14.0/24;
    deny 208.80.20.0/24;
    deny 69.52.192.0/24;
    deny 51.103.0.0/16;
    deny 208.84.0.0/24;
    deny 52.98.16.0/22;
    deny 4.208.0.0/12;
    deny 167.105.0.0/16;
    deny 62.12.60.0/24;
    deny 198.206.164.0/24;
    deny 193.149.64.0/19;
    deny 70.37.0.0/17;
    deny 194.50.21.0/24;
    deny 103.58.119.0/24;
    deny 103.180.108.0/24;
    deny 185.195.245.0/24;
    deny 204.79.252.0/24;
    deny 103.189.128.0/24;
    deny 209.240.192.0/19;
    deny 72.18.78.0/24;
    deny 203.32.11.0/24;
    deny 206.138.168.0/21;
    deny 103.8.80.0/24;
    deny 199.103.90.0/23;
    deny 69.84.180.0/24;
    deny 199.60.28.0/24;
    deny 98.64.0.0/14;
    deny 131.253.62.0/23;
    deny 20.196.0.0/18;
    deny 192.48.225.0/24;
    deny 131.253.1.0/24;
    deny 102.133.0.0/17;
    deny 158.23.0.0/16;
    deny 40.123.192.0/19;
    deny 62.12.56.0/24;
    deny 52.146.0.0/15;
    deny 2.58.103.0/24;
    deny 145.46.161.0/24;
    deny 202.139.240.0/24;
    deny 202.139.248.0/24;
    deny 202.139.241.0/24;
    deny 210.247.217.0/24;
    deny 194.41.22.0/24;
    deny 203.147.139.0/24;
    deny 40.169.0.0/16;
    deny 157.95.0.0/16;
    deny 57.152.0.0/14;
    deny 40.171.0.0/16;
    deny 57.160.0.0/12;
    deny 167.162.0.0/16;
    deny 209.199.0.0/16;
    deny 172.128.0.0/11;
    deny 57.156.0.0/14;
    deny 57.150.0.0/15;
    deny 40.170.0.0/16;
    deny 151.206.0.0/16;

    map $uri $blogname {
         ~^(?P<blogpath>/[^/]+/)files/(.*) $blogpath;
    }

    map $blogname $tmp_blogid {
        default -999;
        include /var/www/html/wp-content/uploads/nginx-helper/map.conf;
    }

    map $http_host $blogid {
        default $tmp_blogid;
        include /var/www/html/wp-content/uploads/nginx-helper/map.conf;
    }

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" "$http_host" '
                      '"$blogid"';
    access_log  /dev/stdout  main;

    server {
        listen 8080;
        listen [::]:8080;

        server_name gnome.org www.gnome.org;

        error_log /dev/stderr;

        absolute_redirect off;

        root /var/www/html;
        index index.php;

        client_max_body_size 100M;

        location /donate/ {
            if ($allowed_country = no) {
                return 444;
            }
            try_files $uri $uri/ /index.php?$args;
        }

        rewrite ^/friends/ https://www.gnome.org/donate/ permanent;
        rewrite ^/support-gnome/ https://www.gnome.org/donate/ permanent;
        rewrite ^/support-gnome/donate https://www.gnome.org/donate/ permanent;

        rewrite ^/(feed|news)$ https://foundation.gnome.org/news/ permanent;
        rewrite ^/(feed|news)/$ https://foundation.gnome.org/news/ permanent;
        rewrite ^/(feed|news)/(.+) https://foundation.gnome.org/news/$1 permanent;
        rewrite ^/logo-and-trademarks/(.*) https://foundation.gnome.org/logo-and-trademarks/$1 permanent;

        rewrite ^/foundation/charter.html https://wiki.gnome.org/Foundation/Charter permanent;
        rewrite ^/foundation/membership/members.php /foundation/membership permanent;
        rewrite ^/vote https://vote.gnome.org permanent;
        rewrite ^/foundation/membership/apply https://gitlab.gnome.org/Teams/MembershipCommittee/issues/new?issuable_template=membership-application permanent;

        rewrite ^/press/releases/ /press/ permanent;
        rewrite ^/community/ /get-involved/ permanent;
        rewrite ^/tour/ / permanent;
        rewrite ^/resources/ / permanent;
        rewrite ^/softwaremap/ / permanent;
        rewrite ^/i18n/ http://wiki.gnome.org/TranslationProject permanent;
        rewrite ^/support/ /support-gnome/ permanent;
        rewrite ^/Friends/ /friends permanent;
        rewrite ^/applications /gnome-3 permanent;
        rewrite ^/friends/other-ways-to-donate /support-gnome/donate permanent;
        rewrite ^/teams/ /get-involved permanent;
        rewrite ^/friends/amazon /support-gnome permanent;

        rewrite ^/css/ https://static.gnome.org/css/ permanent;
        rewrite ^/img/ https://static.gnome.org/img/ permanent;
        rewrite ^/learn/users_guide http://library.gnome.org/users/ permanent;
        rewrite ^/gnome-office https://wiki.gnome.org/GnomeOffice permanent;

        rewrite ^/start/stable https://release.gnome.org/stable permanent;
        rewrite ^/start/unstable/schedule.ics https://static.gnome.org/calendars/schedule-unstable.ics permanent;
        rewrite ^/start/unstable https://wiki.gnome.org/Schedule permanent;
        rewrite ^/start/([0-9]+\.[0-9]*[02468]) https://help.gnome.org/misc/release-notes/$1 permanent;
        rewrite ^/start/([0-9]+\.[0-9]*[13579]) /start/unstable permanent;

        rewrite ^/opw https://outreachy.org permanent;
        rewrite ^/outreachy https://outreachy.org permanent;
        rewrite ^/projects https://projects.gnome.org permanent;

        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        location = /xmlrpc.php {
            deny all;
            access_log off;
            log_not_found off;
            return 444;
        }

        location ~ ^(/[^/]+/)?files/(.+) {
            try_files /wp-content/blogs.dir/$blogid/files/$2 /wp-includes/ms-files.php?file=$2 ;
            expires max;
            access_log off;
            log_not_found off;
        }

        location ~ ^/files/(.*)$ {
            try_files /wp-content/blogs.dir/$blogid/$uri /wp-includes/ms-files.php?file=$1 ;
            expires max;
            access_log off;
            log_not_found off;
        }

        if (!-e $request_filename) {
            rewrite /wp-admin$ $scheme://$host$request_uri/ permanent;
            rewrite ^(/[^/]+)?(/wp-.*) $2 last;
            rewrite ^(/[^/]+)?(/.*\.php) $2 last;
        }

        # avoid php readfile()
        location ^~ /blogs.dir {
            internal;
            alias /var/www/html/wp-content/blogs.dir;
            access_log off;
            log_not_found off;
            expires max;
        }

        location / {
            try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_intercept_errors on;
            fastcgi_read_timeout 300;
            fastcgi_param  HTTPS 'on';
            fastcgi_pass php-fpm;
            fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }

        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
            expires max;
            log_not_found off;
        }

        location ~ ^/foundation/(.*)$ {
            return 301 https://foundation.gnome.org/$1;
        }

        location /.well-known/matrix {
            expires 4h;
            default_type application/json;
            add_header Access-Control-Allow-Origin *;
        }

        location = /.well-known/org.flathub.VerifiedApps.txt {
            return 302 https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/raw/HEAD/data/org.flathub.VerifiedApps.txt;
        }

        location /civicrm/wp-content/plugins/files/civicrm/ConfigAndLog/ {
            deny all;
        }
    }

    server {
        listen 8080 default_server;
        listen [::]:8080 default_server;

        error_log /dev/stderr;

        absolute_redirect off;

        root /var/www/html;
        index index.php;

        rewrite ^/membership/apply https://gitlab.gnome.org/Teams/MembershipCommittee/issues/new?issuable_template=membership-application permanent;
        rewrite ^/charter.html https://wiki.gnome.org/Foundation/Charter permanent;
        rewrite ^/membership/members.php /membership permanent;
        rewrite ^/electionrules.html https://vote.gnome.org/2021/rules.html permanent;
        rewrite ^/vote https://vote.gnome.org permanent;

        location = /bylaws.pdf {
            return 301 https://gitlab.gnome.org/Infrastructure/foundation-web/-/raw/master/foundation.gnome.org/about/bylaws.pdf?inline=false;
        }

        location = /bylaws.rst {
            return 301 https://gitlab.gnome.org/Infrastructure/foundation-web/-/blob/master/foundation.gnome.org/about/bylaws.rst;
        }

        client_max_body_size 100M;

        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        location = /xmlrpc.php {
            deny all;
            access_log off;
            log_not_found off;
            return 444;
        }

        location ~ ^(/[^/]+/)?files/(.+) {
            try_files /wp-content/blogs.dir/$blogid/files/$2 /wp-includes/ms-files.php?file=$2 ;
            expires max;
            access_log off;
            log_not_found off;
        }

        location ~ ^/files/(.*)$ {
            try_files /wp-content/blogs.dir/$blogid/$uri /wp-includes/ms-files.php?file=$1 ;
            expires max;
            access_log off;
            log_not_found off;
        }

        if (!-e $request_filename) {
            rewrite /wp-admin$ $scheme://$host$request_uri/ permanent;
            rewrite ^(/[^/]+)?(/wp-.*) $2 last;
            rewrite ^(/[^/]+)?(/.*\.php) $2 last;
        }

        # avoid php readfile()
        location ^~ /blogs.dir {
            internal;
            alias /var/www/html/wp-content/blogs.dir;
            access_log off;
            log_not_found off;
            expires max;
        }

        location / {
            try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_intercept_errors on;
            fastcgi_read_timeout 300;
            fastcgi_param  HTTPS 'on';
            fastcgi_pass php-fpm;
            fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }

        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
            expires max;
            log_not_found off;
        }
    }
}
